﻿/*
 * PlayerController serves as a Layer for controlling the player.
 */
using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class PlayerController : UnitySingleton<PlayerController> 
	{

		[SerializeField]
		private float m_rotateSpeed;

		[SerializeField]
		private float m_zRotLeft;

		[SerializeField]
		private float m_zRotRight;

		[SerializeField]
		private Gun m_gun;

		[SerializeField]
		public GameObject m_center;

		[SerializeField]
		public bool m_isControllable = true;

		[SerializeField]
		private bool m_isStun;

		[SerializeField]
		private FarmerController m_farmerController;

		[SerializeField]
		private AudioClip m_cleanSound;

		public TimeKeeper m_clock;


		public void loadDefaults ()
		{
			m_clock.startClock();
			NotificationCenter.DefaultCenter.AddObserver(this,"shoot");
			NotificationCenter.DefaultCenter.AddObserver(this,"disableControls");
			NotificationCenter.DefaultCenter.AddObserver(this,"stunControls");
			NotificationCenter.DefaultCenter.AddObserver(this,"enableControls");
		}

		void Start()
		{
			loadDefaults();
		}

		void OnEnable()
		{
			loadDefaults();
		}

		void OnLevelWasLoaded(int level) 
		{
			loadDefaults();
		}

		public void stunControls()
		{
			m_clock.startClock();
			m_clock.IsDone = false;
			m_isControllable = false;
			m_isStun = true;
			//m_farmerController.cleanGun();
		}

		public void disableControls()
		{
			m_isControllable = false;
		}

		public void enableControls()
		{
			m_isControllable = true;
		}

		public void rotate(float p_x, float p_y, float p_z)
		{
			if(m_isControllable) {
				transform.Rotate(0,0,p_z*Time.deltaTime*m_rotateSpeed);
				if( transform.eulerAngles.z > m_zRotLeft) {
					var rot = transform.eulerAngles;
					rot.z = m_zRotLeft;
					transform.eulerAngles = rot;
				}
				if( transform.eulerAngles.z < m_zRotRight) {
					var rot = transform.eulerAngles;
					rot.z = m_zRotRight;
					transform.eulerAngles = rot;
				}
			}
		}

		public void shoot()
		{

			if(m_isControllable) {
				if(m_gun != null) {
					m_gun.fire();
					m_farmerController.shoot();
					NotificationCenter.DefaultCenter.PostNotification(this,"scatter");
				}
			}
		}

		public void move(float p_x, float p_y, float p_z)
		{

		}

		void Update()
		{
			if(m_isControllable == false && m_isStun == true) {
				m_clock.run();
				if(m_clock.m_trigger == true) {
					m_isControllable = true;
					m_isStun = false;
					m_clock.m_trigger = false;
				}
			}
		}

		void OnCollisionEnter2D(Collision2D coll)
		{
			if( coll.gameObject.tag == "Enemy") {
				stunControls();
				m_farmerController.restock();

			} else if(coll.gameObject.tag == "Egg") {

				if(coll.gameObject.GetComponent<Weapon>().IsHit == false) {
					stunControls();
					audio.PlayOneShot(m_cleanSound);
					m_farmerController.cleanGun();
				}
			}
		}

	}
}