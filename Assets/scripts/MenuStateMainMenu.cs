﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class MenuStateMainMenu :  FSMState<MenuSystem> {
	
	static readonly MenuStateMainMenu instance = new MenuStateMainMenu();
	public static MenuStateMainMenu Instance 
	{
		get { return instance; }
	}
	static MenuStateMainMenu() { }
	private MenuStateMainMenu() { }
	
	
	public override void Enter (MenuSystem m)
	{
		Application.LoadLevel(1);
	}
	
	public override void ExecuteOnUpdate (MenuSystem m) 
	{
		
	}
	
	public override void ExecuteOnFixedUpdate (MenuSystem m)
	{
		
	}
	
	public override void Exit(MenuSystem m) 
	{
		
	}
}
