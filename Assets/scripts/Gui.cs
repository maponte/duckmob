﻿/*
 * Author: Misael Aponte
 * Description: A container class to store a gui.
 * Trying to make everything resuable
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames {


	[Serializable]
	public class Gui : PropertyAttribute 
	{
		[SerializeField]
		private string m_name;  //Name so we can keep track of it

		[SerializeField]
		private int m_id;  // Id number in case the name is not what we need

		[SerializeField]
		private bool m_isEnabled;

		[SerializeField]
		private bool m_enableAllChildren;

		[SerializeField]
		private GameObject m_gui;

		[SerializeField]
		private List<GuiSceneSettings> m_sceneSettings;

		#region PROPERTIES

		public string Name
		{
			get { return m_name; }
			set { m_name = value; }
		}

		public int ID
		{
			get { return m_id; }
			set { m_id = value; }
		}

		public List<GuiSceneSettings> SceneSettings
		{
			get { return m_sceneSettings; }
		}

		#endregion


		// Use this for initialization
		public void setDefault () 
		{
			m_gui.SetActive(m_isEnabled);
		}

		public void toggle()
		{
			m_gui.SetActive(!m_gui.activeSelf);
		}

		public void disable()
		{
			m_gui.SetActive(false);
		}

		public void enable()
		{
			m_gui.SetActive(true);
		}

		public void enabler(bool p_set)
		{
			m_gui.SetActive(p_set);
		}
	}
}
