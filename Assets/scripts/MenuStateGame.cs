﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class MenuStateGame :  FSMState<MenuSystem> {
	
	static readonly MenuStateGame instance = new MenuStateGame();
	public static MenuStateGame Instance 
	{
		get { return instance; }
	}
	static MenuStateGame() { }
	private MenuStateGame() { }
	
	
	public override void Enter (MenuSystem m)
	{
		if(m.IsAdBased) {
			Application.LoadLevel(2);
		} else {
			Application.LoadLevel(5);
		}

	}
	
	public override void ExecuteOnUpdate (MenuSystem m) 
	{
		
	}
	
	public override void ExecuteOnFixedUpdate (MenuSystem m)
	{
		
	}
	
	public override void Exit(MenuSystem m) 
	{
		
	}
}
