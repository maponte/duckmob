﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class MenuStateSetup :  FSMState<MenuSystem> {
	
	static readonly MenuStateSetup instance = new MenuStateSetup();
	public static MenuStateSetup Instance 
	{
		get { return instance; }
	}
	static MenuStateSetup() { }
	private MenuStateSetup() { }
	
	
	public override void Enter (MenuSystem m)
	{
		m.setDefaults();
		m.FSM.ChangeState(MenuStateSplash.Instance);

	}
	
	public override void ExecuteOnUpdate (MenuSystem m) 
	{

	}
	
	public override void ExecuteOnFixedUpdate (MenuSystem m)
	{
		
	}
	
	public override void Exit(MenuSystem m) 
	{
		
	}
}
