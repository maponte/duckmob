﻿/*
 * Misael Aponte Feb 5, 2014
 * This handles the events in the game, Such as game objects appearing
 * at a certain time.
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace BoogieDownGames {

	[Serializable]
	public class EventHandler : PropertyAttribute 
	{

		[SerializeField]
		private string m_name;

		[SerializeField]
		public Action[] m_actions;

		[SerializeField]
		private TimeKeeper m_clock;

		public bool m_isDone;

		public TimeKeeper Clock
		{
			get { return m_clock; }
		}

		// Use this for initialization
		public void setUp()
		{
			m_clock.startClock();
			m_isDone = false;
		}

		public void runActions ()
		{
			if(m_clock.m_trigger == true) {
				foreach(Action a in m_actions) {
					a.startAction();
					m_isDone = true;
				}
				m_clock.m_trigger = false;
			}
			m_clock.run();
		}
	}
}