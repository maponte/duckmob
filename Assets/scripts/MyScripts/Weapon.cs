﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Weapon : MonoBehaviour {

		enum WeaponType { Damage, Stun, TakePoints, DamageStun, DamageTakepoints, DamageStunTakePoints, StunTakePoints };

		[SerializeField]
		private WeaponType m_weaponType = WeaponType.Damage;

		[SerializeField]
		private int m_damage;

		[SerializeField]
		private int m_pointDeduction;

		[SerializeField]
		private int m_score;

		[SerializeField]
		private string m_particle;

		[SerializeField]
		private Animator m_anime;

		[SerializeField]
		private float m_delDeath;

		[SerializeField]
		private bool m_isHasHit;

		[SerializeField]
		private Color m_color;

		[SerializeField]
		private float m_fadeSpeed;

		#region PROPERTIES

		public int Damage 
		{
			get { return m_damage; }
		}

		[SerializeField]
		public bool IsHit
		{
			get { return m_isHasHit; }
		}

		#endregion

		// Use this for initialization
		void Start () 
		{
			m_isHasHit = false;
			m_color = Color.white;
			m_color.a = 1;
		}

		void Update()
		{
			if(m_isHasHit == true) {
				m_color.a -= m_fadeSpeed;
				GetComponent<SpriteRenderer>().color = m_color;
			}

			if(m_color.a <= 0) {
				gameObject.SetActive(false);
			}
		}

		void OnEnable()
		{
			m_isHasHit = false;
			m_color.a = 1;
			gameObject.GetComponent<SpriteRenderer>().color = m_color;
		}

		void runWeaponType(Collision2D p_coll)
		{
			if(p_coll.gameObject.name == "Player" && m_isHasHit == false) {
				m_isHasHit = true;
				if(m_anime != null) {
					m_anime.SetTrigger("break");
					//StartCoroutine(delayDeath(m_delDeath));
				}

				if(m_particle != null) {
					GameObject obj = MemoryPool.Instance.findAndGetObjs(m_particle,false);
					obj.SetActive(true);
					obj.transform.position = transform.position;
					obj.particleSystem.Play();
				}
			}
			if(p_coll.gameObject.tag == "Bullet" && m_isHasHit == false) {
				//p_coll.gameObject.SetActive(false);
				m_anime.SetTrigger("break");
				m_isHasHit = true;
				//StartCoroutine(delayDeath(0.5f));
				p_coll.gameObject.SetActive(false);
			} else if(p_coll.gameObject.tag == "Bullet") {
				p_coll.gameObject.SetActive(false);
			}
		}

		void OnCollisionEnter2D(Collision2D coll)
		{
			runWeaponType(coll);
		}

		IEnumerator delayDeath(float p_sec)
		{
			yield return new WaitForSeconds(p_sec);
			gameObject.SetActive(false);
		}

	}
}