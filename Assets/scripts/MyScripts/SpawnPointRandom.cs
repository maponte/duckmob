﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames{

	public class SpawnPointRandom : MonoBehaviour {

		[SerializeField]
		private string m_objName;

		[SerializeField]
		private GameObject m_objDopIn;

		[SerializeField]
		TimeKeeper m_clock;


		[SerializeField]
		private float m_min;

		[SerializeField]
		private float m_max;


		[SerializeField]
		private bool m_setRandomVelocity;

		[SerializeField]
		private float m_speedMin;

		[SerializeField]
		private float m_speedMax;

		[SerializeField]
		private bool m_isLayerRandom;
		[SerializeField]
		private int m_layerRanLimit;




		// Use this for initialization
		void Start () 
		{
			m_clock.startClock ();
			m_objName = m_objDopIn.name;

		}
		
		// Update is called once per frame
		void Update () 
		{
			if (m_clock.m_trigger) {
				m_clock.TimeLimit =  Random.Range(m_min,m_max);
				GameObject obj = MemoryPool.Instance.findAndGetObjs(m_objName,false);
				if(obj != null) {
					obj.transform.position = transform.position;

					if(m_setRandomVelocity) {
						obj.rigidbody2D.velocity = new Vector2(Random.Range(m_speedMin, m_speedMax),0);
					}
					if(m_isLayerRandom == true) {
						obj.layer = Random.Range(0, m_layerRanLimit);
					}

					m_clock.m_trigger = false;


				}
			}
			m_clock.run ();
		}
	}
}