﻿/*
 * Misael Aponte
 * Description: The egg component any weapon with this componet will have
 * behavior like an egg.
 * 
 */

using UnityEngine;
using System.Collections;

namespace BoogieDownGames {
	public class Egg : MonoBehaviour {

		[SerializeField]
		private string m_collisionTag;

		[SerializeField]
		private float m_fadeTime;

		[SerializeField]
		private float m_distanceFromTarget;

		[SerializeField]
		private TimeKeeper m_counter;

		[SerializeField]
		private GameObject m_player;
	

		// Use this for initialization
		void Start () 
		{
			m_player = GameObject.Find("Player");
			transform.localScale = new Vector3(1,1,1);
		}

		void OnEnable()
		{
			transform.localScale = new Vector3(1,1,1);
		}
		
		// Update is called once per frame
		void Update () 
		{

			m_distanceFromTarget = Mathf.Abs( Vector3.Distance(m_player.transform.position,transform.position) );
			Debug.LogError("1st => " + m_distanceFromTarget);
			m_distanceFromTarget = Mathf.Abs( 20 - m_distanceFromTarget );
			m_distanceFromTarget *=0.1f;
			transform.localScale = new Vector3(m_distanceFromTarget,m_distanceFromTarget,m_distanceFromTarget);
		}
	}
}