﻿using UnityEngine;
using System.Collections;


namespace BoogieDownGames 
{
	public class AppOverRide : MonoBehaviour {

		[SerializeField]
		private bool m_canTransition;

		[SerializeField]
		private float m_delay;

		[SerializeField]
		private MenuSystem m_menu;

		// Use this for initialization
		void Start () 
		{
			m_menu = GameObject.Find("GameGui").GetComponent<MenuSystem>();
			StartCoroutine(startDelay(m_delay));
		}
		
		// Update is called once per frame
		void Update () 
		{
			if(m_canTransition== true) {
				m_menu.FSM.ChangeState(MenuStateMainMenu.Instance);
			}
		}

		IEnumerator startDelay(float p_delay)
		{
			yield return new WaitForSeconds(p_delay);
			m_canTransition = true;
		}
	}
}