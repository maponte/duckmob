﻿/*
 * Misael Aponte Feb 5, 2014
 * Simple Ai for the ducks
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BoogieDownGames {

	[RequireComponent (typeof (AudioSource))]
	[RequireComponent (typeof (Animator))]
	public class DuckAi : MonoBehaviour 
	{

		public enum AIType { Happy, Scatterer, Dropper, Pooper, Kamikaze, Chicken, RandomAI };
		[SerializeField]
		public AIType m_aiType = AIType.Happy;

		[SerializeField]
		private GameObject m_target;

		[SerializeField]
		private Vector2 m_velScared;

		[SerializeField]
		private Vector2 m_velHappy;

		[SerializeField]
		private string m_duckWeapon;

		[SerializeField]
		private float m_speed;

		[SerializeField]
		private float m_scaredSpeed;

		public bool m_isDead = false;

		[SerializeField]
		private int m_score;

		[SerializeField]
		private AudioClip m_duckDeath;

		[SerializeField]
		private AudioClip m_duckQuack;

		private FiniteStateMachine<DuckAi> m_fsm = new FiniteStateMachine<DuckAi>();

		[SerializeField]
		public Animator m_animeController;

		[SerializeField]
		public int m_health;

		[SerializeField]
		public int m_initHealth;

		[SerializeField]
		private bool m_isFacingRight;

		[Range(-10.0F, 10.0F)]
		public float m_xScatterRngMin;

		[Range(-10.0F, 10.0F)]
		public float m_xScatterRngMax;

		[Range(-10.0F, 10.0F)]
		public float m_yScatterRngMin;

		[Range(-10.0F, 10.0F)]
		public float m_yScatterRngMax;

		[Range(0.0F, 100.0F)]
		public float m_hearingRange;



		[SerializeField]
		private float m_setLayerTimer;

		[SerializeField]
		public string m_afterEffect;

		[SerializeField]
		public int m_damage;


		public TimeKeeper m_clock;

		#region PROPERTIES

		public FiniteStateMachine<DuckAi> FSM 
		{
			get { return m_fsm; }
			set { m_fsm = value; }
		}

		public int SCORE
		{
			get { return m_score; }
			set { m_score = value; }
		}

		public bool IsDead
		{
			get { return m_isDead; }
			set { m_isDead = value; }
		}

		public Vector2 HappyVel
		{
			get { return m_velHappy; }
			set { m_velHappy = value; } 
		}

		#endregion


		public void react(AIType p_type)
		{
			switch(p_type) {

			case AIType.Happy:
				//run the simple state it just keeps flying
				FSM.ChangeState(DuckStateHappy.Instance);
				break;

			case AIType.Scatterer:
				//Duck scatters upon hearing report
				FSM.ChangeState(DuckStateScatter.Instance);
				break;

			case AIType.Chicken:
				//Duck flys really fast
				FSM.ChangeState(DuckStateChicken.Instance);
				break;

			case AIType.Dropper:
				//Duck drops a rock
				FSM.ChangeState(DuckStateDropper.Instance);
				break;

			case AIType.Kamikaze:
				//Duck physically flys into the farmer
				FSM.ChangeState(DuckStateKamakize.Instance);
				break;

			case AIType.Pooper:
				//Duck drops poop
				FSM.ChangeState(DuckStatePooper.Instance);
				break;
			}
		}


		void Awake()
		{
			m_animeController = GetComponent<Animator>();
			m_animeController.animatePhysics = true;
			m_animeController.applyRootMotion = false;
		}

		public void setInitialDuck()
		{
			//reset the clock
			GetComponent<BoxCollider2D>().enabled = true;
			IsDead = false;
			rigidbody2D.gravityScale = 0;
			resetScale();
			m_animeController = GetComponent<Animator>();
			m_animeController.animatePhysics = true;
			m_animeController.applyRootMotion = false;
			rigidbody2D.fixedAngle = true;
			gameObject.collider2D.enabled = true;
			m_health = m_initHealth;
			m_fsm.Configure(this,DuckStateHappy.Instance);
			NotificationCenter.DefaultCenter.AddObserver(this,"scatter");
			NotificationCenter.DefaultCenter.AddObserver(this,"changeStateFlyOff");
		}

		// Use this for initialization
		void Start () 
		{
			setInitialDuck();
			//iTween.MoveTo(gameObject,iTween.Hash("path",iTweenPath.GetPath("path1"),"time",5));

		}

		void OnEnable ()
		{
			setInitialDuck();
			StartCoroutine (setLayer (17, m_setLayerTimer));

			randomPlay();
		}

		public void randomPlay()
		{
			int num1 = Random.Range(1,10000);
			int num2 = Random.Range(1,10000);
			int mod = num1 % num2;
			if(mod == 1) {
				audio.Play();
			}
		}

		void Update ()
		{
			m_animeController.SetFloat("speedX",rigidbody2D.velocity.x * (0.1f) );
			m_animeController.SetFloat("speedY",rigidbody2D.velocity.y * (0.1f));
			m_animeController.SetBool("dead",IsDead);
			m_fsm.runOnUpdate();
		}

		public void resetScale()
		{
			Vector3 scale = transform.localScale;
			scale.x = 1.0f;
			scale.y = 1.0f;
			scale.z = 1.0f;
			transform.localScale = scale;
		}

		void FixedUpdate ()
		{
			m_fsm.runOnFixedUpdate();
		}

		public void flyStateGoal()
		{
			rigidbody2D.velocity = m_velHappy;
		}

		public void fall()
		{

		}

		public void scatter()
		{
			if(m_aiType == AIType.Scatterer && m_health > 0) {
				StartCoroutine(setScatter(0.2f));
			}
		}

		IEnumerator setScatter (float p_sec)
		{
			yield return new WaitForSeconds(p_sec);
			m_fsm.ChangeState(DuckStateScatter.Instance);
		}

		public void setScatterVelocity()
		{
			m_velScared =  (new Vector2(Random.Range(m_xScatterRngMin,m_xScatterRngMax),Random.Range(m_yScatterRngMin,m_yScatterRngMax)));
		}

		public void flyStateScatter()
		{
			rigidbody2D.velocity = m_velScared;
		}

		void OnCollisionEnter2D(Collision2D coll)
		{
			if(coll.gameObject.tag == "Bullet") {
			
				m_health -= coll.gameObject.GetComponent<Bullet>().Damage;
				coll.gameObject.SetActive(false);
				setGravity(0.5f);
			} else if(coll.gameObject.name == "Player") {
				FSM.ChangeState(DuckStateFlyOff.Instance);
				GameObject obj =  MemoryPool.Instance.findAndGetObjs("Wham",false);
				if(obj != null) {
					obj.transform.position = transform.position;
				}
			}
		}


		public void playQuack()
		{
			if( m_duckQuack != null) {
				audio.PlayOneShot( m_duckQuack );
			}
		}

		public void playDeathQuack()
		{
			if(m_duckDeath != null) {
				audio.PlayOneShot( m_duckDeath );
			}
		}

		public bool dropWeapon()
		{
			bool dropped = false;
			//grab the weapon and target
			GameObject wpn = MemoryPool.Instance.findAndGetObjs(m_duckWeapon,false);
			if(wpn != null) {
				wpn.SetActive(false);
				GameObject target = GameObject.Find("Player");
				if(target != null) { 
					if( Mathf.Abs( (transform.position.x - target.transform.position.x) ) <= 4) {
						wpn.transform.position = transform.position;
						wpn.SetActive(true);
						wpn.rigidbody2D.velocity = rigidbody2D.velocity;
						dropped = true;
					}
				}
			}
			return dropped;
		}

		public void flyOff()
		{
			Vector2 vel = HappyVel;
			vel.y = 4.0f;
			rigidbody2D.velocity = vel;
		}

		public void checkForDeath()
		{
			if(m_health <= 0) {
				FSM.ChangeState(DuckStateHit.Instance);
			}
		}

		public void kamikaze()
		{
			GameObject target = GameObject.Find("Player");
			if(target != null) {
				Vector3 vel = target.transform.position - transform.position;
				Vector2 v = new Vector2(vel.x,vel.y);
				rigidbody2D.velocity = v * 2;
			}
		}

		IEnumerator setLayer(int p_layer, float p_sec)
		{
			yield return new WaitForSeconds(p_sec);
			gameObject.layer = p_layer;
		}

		public void setGravity(float p_sec)
		{
			StartCoroutine(setGravCorotuine(p_sec));
		}

		IEnumerator setGravCorotuine ( float p_sec)
		{
			yield return new WaitForSeconds(p_sec);
			rigidbody2D.gravityScale = 1;
		}

		public void changeStateFlyOff()
		{
			FSM.ChangeState(DuckStateEndRound.Instance);
		}


	}
}