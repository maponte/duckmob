﻿/*
 * Misael Aponte Feb 4, 2014
 * The Game Master Script; Controls the game flow
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BoogieDownGames {

	public class GameMaster : UnitySingletonPersistent<GameMaster>
	{
		enum GameLevel { Easy, Med, Hard, Harder, Killer };

		[SerializeField]
		private GameLevel m_gameLevel = GameLevel.Easy;

		[SerializeField]
		private bool m_isPaused;

		[SerializeField]
		private FiniteStateMachine<GameMaster> m_fsm = new FiniteStateMachine<GameMaster>();

		[SerializeField]
		private int m_playerScore;

		[SerializeField]
		private int m_highScore;

		[SerializeField]
		private int m_currentNumOfEnemies;

		[SerializeField]
		private int m_maxNumOfEnemies;

		[SerializeField]
		public TimeKeeper m_splashScreenTimer;

		[SerializeField]
		public List<AudioClip> m_sounds;

		[SerializeField]
		public AudioSource m_audioSource;

		[SerializeField]
		public int m_ammo;
		
		[SerializeField]
		public int m_ammoMax;
		
		[SerializeField]
		public int m_ammoBag;

		[SerializeField]
		public TextMachine m_scoreKeeper;

		[SerializeField]
		public TextMachine m_highScoreKeeper;

		[SerializeField]
		public TextMachine m_birdKeeper;

		[SerializeField]
		public string m_saveFileName;

		[SerializeField]
		private string m_adId;


		public FiniteStateMachine<GameMaster> FSM
		{
			get { return m_fsm; }
			set { m_fsm = value; }
		}

		#region Properties


		public int PlayerScore
		{
			get{ return m_playerScore; }
			set { m_playerScore = value; }
		}

		public int CurrentNumOfEnemies
		{
			get { return m_currentNumOfEnemies; }
			set { m_currentNumOfEnemies = value; }
		}

		public int MaxNumOfEnemies
		{
			get { return m_maxNumOfEnemies; }
			set { m_maxNumOfEnemies = value; }
		}

		public int HighScore
		{
			get { return m_highScore; }
			set { m_highScore = value; }
		}
		#endregion

		// Use this for initialization
		void Start ()
		{

			//Always start off in the splash state
			m_fsm.Configure(this,GameStateSplash.Instance);
			m_audioSource = GetComponent<AudioSource>();
			load();
		}

		public void resetBirdCounter()
		{
			m_currentNumOfEnemies = 0;
		}

		public void updateText()
		{
			this.resetBirdCounter();
		}
		
		// Update is called once per frame
		void Update () 
		{

			updateTexts();
			m_fsm.runOnUpdate();
		}

		void FixedUpdate ()
		{
			m_fsm.runOnFixedUpdate();
		}

		public void sendMessage(string p_funcCall, string p_id, string p_data)
		{
			Hashtable dat = new Hashtable();
			dat.Add("stringID",p_id);
			dat.Add("data",p_data);
			NotificationCenter.DefaultCenter.PostNotification(this,p_funcCall,dat);
		}


		public void updateTexts()
		{

			if(m_playerScore > m_highScore) {
				m_highScore = m_playerScore;
			}

			if( m_scoreKeeper != null) {
				m_scoreKeeper.LabelText =   String.Format("{0:0000}", m_playerScore);  
			}

			if( m_highScoreKeeper != null) {
				m_highScoreKeeper.LabelText = String.Format("{0:0000}", m_highScore);  
			}

			if( m_birdKeeper != null) {
				m_birdKeeper.LabelText = String.Format("{0:00}", m_currentNumOfEnemies); 
			}
		}

		public void playAudio(int p_index)
		{
			m_audioSource.PlayOneShot(m_sounds[p_index]);
		}

		public void save()
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + "gameSave.dat",FileMode.Create);
			SaveData dat = new SaveData();
			dat.m_playerHighScore = m_highScore;

			bf.Serialize(file,dat);
			file.Close();

		}

		public void load()
		{
			if(File.Exists(Application.persistentDataPath + "/" + "gameSave.dat")) {

				BinaryFormatter bf = new BinaryFormatter();
				FileStream fs = File.Open(Application.persistentDataPath + "/" + "gameSave.dat",FileMode.Open);
				SaveData dat = (SaveData)bf.Deserialize(fs);
				fs.Close();
				m_highScore = dat.m_playerHighScore;
			}

			NotificationCenter.DefaultCenter.AddObserver(this,"updateText");
		}

		public void resetPlayer()
		{
			m_currentNumOfEnemies = 0;
			m_playerScore = 0;
		}

		IEnumerator GameOverDelay (float p_sec)
		{
			yield return new WaitForSeconds(p_sec);
			FSM.ChangeState(GameStateMenu.Instance);

		}

		void OnLevelWasLoaded(int level) 
		{
			NotificationCenter.DefaultCenter.AddObserver(this,"updateText");
			Debug.LogError("Ive loaded " + level);
			if(level == 0) {
				load();
				FSM.ChangeState(GameStatePlay.Instance);
			} else if(level == 3 || level == 4 || level == 5) {

				save();
			}
		}

	}



	[Serializable]
	public class SaveData
	{
		public int m_playerLastScore;
		public int m_playerHighScore;
	}
}