﻿using UnityEngine;
using System.Collections;

public class MenuSet : MonoBehaviour 
{

	void Start()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,"toogleEnable");
	}

	void Enabled()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,"toogleEnable");
	}

	public void toogleEnable()
	{
		gameObject.SetActive( !gameObject.activeSelf);
	}
}
