/*
 *	Game State Splash
 *	This should be used to handle the Splash screen
 */
using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStateSplash :  FSMState<GameMaster> {
	
	static readonly GameStateSplash instance = new GameStateSplash();
	public static GameStateSplash Instance
	{
		get { return instance; }
	}
	
	static GameStateSplash() { }
	private GameStateSplash() { }
		
	public override void Enter (GameMaster m)
	{
		Application.LoadLevel(0);
		m.m_splashScreenTimer.startClock();
		m.playAudio(0);

	}
	
	public override void ExecuteOnUpdate (GameMaster m)
	{

	}

	public override void ExecuteOnFixedUpdate(GameMaster m)
	{

	}
	
	public override void Exit(GameMaster m) 
	{

	}
}
