﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStatePooper :  FSMState<DuckAi> {
	
	static readonly DuckStatePooper instance = new DuckStatePooper();
	public static DuckStatePooper Instance 
	{
		get { return instance; }
	}
	static DuckStatePooper() { }
	private DuckStatePooper() { }
	
	
	public override void Enter (DuckAi p_duck)
	{
		//Get the vector for the target
		p_duck.dropWeapon();
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.checkForDeath();
		p_duck.flyStateGoal();
		p_duck.resetScale();
		if(p_duck.dropWeapon()) {
			p_duck.FSM.ChangeState(DuckStateFlyOff.Instance);
		}
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}