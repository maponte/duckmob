﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateKamakize :  FSMState<DuckAi> {
	
	static readonly DuckStateKamakize instance = new DuckStateKamakize();
	public static DuckStateKamakize Instance 
	{
		get { return instance; }
	}
	static DuckStateKamakize() { }
	private DuckStateKamakize() { }
	
	
	public override void Enter (DuckAi p_duck)
	{
		//Get the vector for the target
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.checkForDeath();
		p_duck.resetScale();
		p_duck.kamikaze();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}