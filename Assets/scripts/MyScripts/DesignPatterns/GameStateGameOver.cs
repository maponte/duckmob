using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStateGameOver :  FSMState<GameMaster> {
	
	static readonly GameStateGameOver instance = new GameStateGameOver();
	public static GameStateGameOver Instance 
	{
		get { return instance; }
	}
	static GameStateGameOver() { }
	private GameStateGameOver() { }
	
		
	public override void Enter (GameMaster m)
	{
		Application.LoadLevel(3);
	}
	
	public override void ExecuteOnUpdate (GameMaster m) 
	{


	}

	public override void ExecuteOnFixedUpdate (GameMaster m)
	{

	}
	
	public override void Exit(GameMaster m) 
	{
		
		Debug.Log("Exiting game over");
	}
}