using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStatePause :  FSMState<GameMaster> {
	
	static readonly GameStatePause instance = new GameStatePause();
	public static GameStatePause Instance 
	{
		get { return instance; }
	}
	static GameStatePause() { }
	private GameStatePause() { }
	
		
	public override void Enter (GameMaster m) 
	{

	}
	
	public override void ExecuteOnUpdate (GameMaster m) 
	{
	
	}

	public override void ExecuteOnFixedUpdate (GameMaster m)
	{

	}
	
	public override void Exit(GameMaster m) {
	}
}