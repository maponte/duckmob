﻿/*
 *	Game State Menu
 *	This should be used to handle the Menu Scene
 */
using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStateMenu :  FSMState<GameMaster> {
	
	static readonly GameStateMenu instance = new GameStateMenu();
	public static GameStateMenu Instance
	{
		get { return instance; }
	}
	
	static GameStateMenu() { }
	private GameStateMenu() { }
	
	public override void Enter (GameMaster m)
	{
		Application.LoadLevel(1);
		Debug.LogError ("Im in menu State");
	}

	public override void ExecuteOnUpdate (GameMaster entity)
	{


	}

	public override void ExecuteOnFixedUpdate (GameMaster entity)
	{

	}

	public override void Exit (GameMaster entity)
	{

	}

}