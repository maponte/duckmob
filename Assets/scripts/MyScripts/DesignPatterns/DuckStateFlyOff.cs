﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateFlyOff :  FSMState<DuckAi> {
	
	static readonly DuckStateFlyOff instance = new DuckStateFlyOff();
	public static DuckStateFlyOff Instance 
	{
		get { return instance; }
	}
	static DuckStateFlyOff() { }
	private DuckStateFlyOff() { }
	
	
	public override void Enter (DuckAi p_duck)
	{

	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.resetScale ();

		p_duck.flyOff();
		p_duck.checkForDeath();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}