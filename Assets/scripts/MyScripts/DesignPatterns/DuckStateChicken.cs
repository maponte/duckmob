﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateChicken :  FSMState<DuckAi> {
	
	static readonly DuckStateChicken instance = new DuckStateChicken();
	public static DuckStateChicken Instance 
	{
		get { return instance; }
	}
	
	static DuckStateChicken() { }
	private DuckStateChicken() { }
	
	public override void Enter (DuckAi p_duck)
	{
		p_duck.resetScale();
		Vector2 vel = p_duck.HappyVel;
		vel.x *= 4;
		p_duck.rigidbody2D.velocity = vel;
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.resetScale();
		p_duck.rigidbody2D.velocity = p_duck.HappyVel * 4;

		p_duck.checkForDeath();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{

	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}