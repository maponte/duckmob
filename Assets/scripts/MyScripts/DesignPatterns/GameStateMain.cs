/* the main game menu
 * 
 */
using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStateMain :  FSMState<GameMaster> {
	
	static readonly GameStateMain instance = new GameStateMain();
	public static GameStateMain Instance
	{
		get { return instance; }
	}
	
	static GameStateMain() { }
	private GameStateMain() { }
	
		
	public override void Enter (GameMaster m) 
	{
		Debug.LogError("entering game state main");
	}
	
	public override void ExecuteOnUpdate (GameMaster m) 
	{

	}

	public override void ExecuteOnFixedUpdate (GameMaster m)
	{
	}

	
	public override void Exit(GameMaster m) 
	{
		Debug.LogError("exiting game state main");
	}
}