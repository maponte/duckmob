using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStatePlay :  FSMState<GameMaster> {
	
	static readonly GameStatePlay instance = new GameStatePlay();
	public static GameStatePlay Instance 
	{
		get { return instance; }
	}
	
	static GameStatePlay() { }
	private GameStatePlay() { }

	public override void Enter (GameMaster m) 
	{
		m.resetPlayer();
	}
	
	public override void ExecuteOnUpdate (GameMaster m) 
	{
		m.updateTexts();
		/*
		m.sendMessage("updateText","score",m.PlayerScore.ToString());
		m.sendMessage("updateText","scoreHigh",m.HighScore.ToString());
		m.sendMessage("updateText","currentEnemies",m.CurrentNumOfEnemies.ToString());
		m.sendMessage("updateText","maxEnemies",m.MaxNumOfEnemies.ToString());
		*/
	}

	public override void ExecuteOnFixedUpdate (GameMaster m)
	{

	}
	
	public override void Exit(GameMaster m)
	{

	}
}
