﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateDropper :  FSMState<DuckAi> {
	
	static readonly DuckStateDropper instance = new DuckStateDropper();
	public static DuckStateDropper Instance 
	{
		get { return instance; }
	}
	static DuckStateDropper() { }
	private DuckStateDropper() { }

	public override void Enter (DuckAi p_duck)
	{

	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.checkForDeath();
		p_duck.flyStateGoal();
		p_duck.resetScale();
		if(p_duck.dropWeapon()) {
			p_duck.FSM.ChangeState(DuckStateFlyOff.Instance);
		}

	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}