﻿/*
 * The duck happy state
 * 
 */
using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateHappy :  FSMState<DuckAi> {
	
	static readonly DuckStateHappy instance = new DuckStateHappy();
	public static DuckStateHappy Instance 
	{
		get { return instance; }
	}

	static DuckStateHappy() { }
	private DuckStateHappy() { }

	public override void Enter (DuckAi p_duck)
	{
		p_duck.resetScale();
		p_duck.m_clock.startClock();
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.flyStateGoal();
		p_duck.resetScale();
		p_duck.m_clock.run();
		p_duck.checkForDeath();

		//Check for the clock
		if(p_duck.m_clock.IsDone) {
			p_duck.m_clock.IsDone = false;
			p_duck.react(p_duck.m_aiType);
		}

	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		p_duck.randomPlay();
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		

	}
}