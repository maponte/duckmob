﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateHit :  FSMState<DuckAi> {
	
	static readonly DuckStateHit instance = new DuckStateHit();
	public static DuckStateHit Instance 
	{
		get { return instance; }
	}
	static DuckStateHit() { }
	private DuckStateHit() { }
	
	
	public override void Enter (DuckAi p_duck)
	{
		p_duck.IsDead = true;
		p_duck.GetComponent<BoxCollider2D>().enabled = false;
		p_duck.rigidbody2D.velocity = new Vector2(p_duck.rigidbody2D.velocity.x, 5);
		GameMaster.Instance.PlayerScore += p_duck.SCORE;
		GameMaster.Instance.CurrentNumOfEnemies++;
		LevelManager.Instance.incrementCurrentRoundKills();
		p_duck.rigidbody2D.fixedAngle = false;
		p_duck.playDeathQuack();

		if( p_duck.m_afterEffect.Length > 0) {
			GameObject obj = MemoryPool.Instance.findAndGetObjs(p_duck.m_afterEffect,false);
			if(obj != null) {
				obj.transform.position = p_duck.gameObject.transform.position;
				obj.SetActive(true);
			}
		}
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.IsDead = true;
		//p_duck.fall();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}