﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateScatter :  FSMState<DuckAi> {
	
	static readonly DuckStateScatter instance = new DuckStateScatter();
	public static DuckStateScatter Instance 
	{
		get { return instance; }
	}
	
	static DuckStateScatter() { }
	private DuckStateScatter() { }
	
	public override void Enter (DuckAi p_duck)
	{
		p_duck.resetScale();

		p_duck.setScatterVelocity();
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.resetScale();
		p_duck.flyStateScatter();
		p_duck.checkForDeath();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}