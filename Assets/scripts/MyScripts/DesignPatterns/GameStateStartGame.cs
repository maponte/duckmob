﻿/*
 *	Game State Start Game
 *	Starts off the game
 */
using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class GameStateStartGame :  FSMState<GameMaster>
{
	
	static readonly GameStateStartGame instance = new GameStateStartGame();
	public static GameStateStartGame Instance
	{
		get { return instance; }
	}
	
	static GameStateStartGame() { }
	private GameStateStartGame() { }
	
	public override void Enter (GameMaster m)
	{
		Application.LoadLevel(2);
	}
	
	public override void ExecuteOnUpdate (GameMaster m)
	{
		m.sendMessage("updateText","score",m.PlayerScore.ToString());
	}
	
	public override void ExecuteOnFixedUpdate(GameMaster m)
	{
		
	}
	
	public override void Exit(GameMaster m) 
	{

	}
}
