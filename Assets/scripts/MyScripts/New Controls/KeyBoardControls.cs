﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class KeyBoardControls : MonoBehaviour {

		[SerializeField]
		private PlayerController m_controls;
		public float fireRate = 0.5F;
		private float nextFire = 0.0F;
	


		// Use this for initialization
		void Start () 
		{
		
		}
		
		// Update is called once per frame
		void Update () 
		{
			m_controls.rotate(0,0, Input.GetAxis("Horizontal"));
			m_controls.rotate(0,0, Input.GetAxis("Mouse X"));
			if (Input.GetButton("Fire1") && Time.time > nextFire) {
				nextFire = Time.time + fireRate;
				m_controls.shoot();
			}

		}
	}
}