﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames {

	[Serializable]
	public class Scale : PropertyAttribute 
	{
		[SerializeField]
		private string m_title;

		[SerializeField]
		private float m_xScale;

		[SerializeField]
		private float m_yScale;

		[SerializeField]
		private float m_zScale;

		[SerializeField]
		private int m_width;

		[SerializeField]
		private int m_height;

		#region Properties

		public float XScale
		{
			get { return m_xScale; }
			set { m_xScale = value; }  
		}

		public float YScale 
		{
			get { return m_yScale; }
			set { m_yScale = value; }
		}

		public float ZScale
		{
			get { return m_zScale; }
			set { m_zScale = value; }
		}

		public int Width
		{
			get { return m_width; }
		}

		public int Height
		{
			get { return m_height; }
		}

		#endregion

	}
}