﻿using UnityEngine;
using System.Collections;


namespace BoogieDownGames {

public class HealthSystem : MonoBehaviour {

		[SerializeField]
		private Animator m_anime;

		[SerializeField]
		public int m_health;

		// Use this for initialization
		void Start () 
		{
			NotificationCenter.DefaultCenter.AddObserver(this,"setHealth");
		}
		
		// Update is called once per frame
		void Update () 
		{
			m_anime.SetInteger("health",m_health);
		}

		public void setHealth(NotificationCenter.Notification p_not)
		{
			m_health = (int)p_not.data["health"];
		}
	}
}
