﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class MenuStateSplash :  FSMState<MenuSystem> {
	
	static readonly MenuStateSplash instance = new MenuStateSplash();
	public static MenuStateSplash Instance 
	{
		get { return instance; }
	}
	static MenuStateSplash() { }
	private MenuStateSplash() { }
	
	
	public override void Enter (MenuSystem m)
	{
		m.Clock.startClock();
	}
	
	public override void ExecuteOnUpdate (MenuSystem m) 
	{
		if(Input.anyKeyDown) {
			m.FSM.ChangeState(MenuStateMainMenu.Instance);
		}
		m.Clock.run();
		if(m.Clock.IsDone == true) {
			m.FSM.ChangeState(MenuStateMainMenu.Instance);
		}
	}
	
	public override void ExecuteOnFixedUpdate (MenuSystem m)
	{
		
	}
	
	public override void Exit(MenuSystem m) 
	{

	}
}
