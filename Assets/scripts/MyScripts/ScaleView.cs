﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace BoogieDownGames {

	public class ScaleView : MonoBehaviour {

		[SerializeField]
		private List<Scale> m_scales;



		// Use this for initialization
		void Start () 
		{
			checkResolution();
		}

		void OnEnbable ()
		{
			checkResolution();
		}
		
		// Update is called once per frame
		void Update () 
		{

		}

		void FixedUpdate()
		{
			checkResolution();
		}

		public void checkResolution()
		{

			foreach(Scale scale in m_scales) {
				if( scale.Width == Screen.width && scale.Height == Screen.height ) {
					//Debug.LogError(scale.XScale + " " + scale.YScale + " " + scale.ZScale + "   " +  Screen.width + "  " + Screen.height);
					transform.localScale = new Vector3(scale.XScale,scale.YScale, scale.ZScale);

				}
			}
		}
	}
}