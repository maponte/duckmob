﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Gunsmoke : MonoBehaviour {

		[SerializeField]
		private float m_opacity;

		[SerializeField]
		private float m_dissapateSpeed;

		[SerializeField]
		private SpriteRenderer m_render;

		// Use this for initialization
		void Start () 
		{
		
		}

		void OnEnable()
		{
			m_opacity = 1f;
		}
		
		// Update is called once per frame
		void Update () 
		{
			m_opacity -= m_dissapateSpeed;

			m_render.color = new Color (1f, 1f, 1f, m_opacity);
			if (m_opacity < 0.0f) {
				gameObject.SetActive(false);
			}
		}
	}
}