﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class GunClip : UnitySingleton<GunClip> {

		[SerializeField]
		private int m_clips; //This is the total amount of clips for this type of ammo so that I don't have to instantiate a ton of objects

		[SerializeField]
		private int m_currentAmmo;

		[SerializeField]
		private int m_ammoCapacity;

		[SerializeField]
		private GameObject m_bullet;

		[SerializeField]
		private string m_bulletName;

		enum BulletType { Obj, String };
		BulletType m_callingType = BulletType.String;

		[SerializeField]
		private UILabel m_label;
	

		#region Properties

		public int CurrentAmmo
		{
			get { return m_currentAmmo; }
			set { m_currentAmmo = value; }
		}

		public int AmmoCapacity
		{
			get { return m_ammoCapacity; }
			set { m_ammoCapacity = value; }
		}

		public string BulletName
		{
			get { return m_bulletName; }
			set { m_bulletName = value; }
		}

		public int Clips
		{
			get { return m_clips; }
			set { m_clips = value; }
		}

		#endregion

		public bool empty()
		{
			return m_currentAmmo > 0 ? false : true;
		}

		public void reload()
		{
			if(m_clips > 0) {
				m_clips--;
				m_currentAmmo = m_ammoCapacity;
			}
		}

		public GameObject feedBullet()
		{
			return MemoryPool.Instance.findAndGetObjs(this.m_bulletName,false);
		}

		void FixedUpdate ()
		{
			m_label.text = m_currentAmmo.ToString();
		}

		void sendMessage(string p_funcCall, string p_id, string p_data)
		{
			Hashtable dat = new Hashtable();
			dat.Add("stringID",p_id);
			dat.Add("intID",99);
			dat.Add("data",p_data);
			NotificationCenter.DefaultCenter.PostNotification(this,p_funcCall,dat);
		}

		public void reload(NotificationCenter.Notification p_not)
		{
			GetComponent<AudioSource>().Play();
		}


	}
}