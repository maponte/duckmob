﻿/*
 * Controls the level and what criteria the player needs in order to 
 * win/lose the round/game
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames {

	[System.Serializable]
	public class LevelManager : UnitySingleton<LevelManager> 
	{
		[SerializeField]
		private int m_numberOfRounds;	//How many rounds in the the level

		[SerializeField]
		private int m_currentRound; //What round are we in

		[SerializeField]
		private int m_enemiesKilledTotal; //Total number of enemies killed

		[SerializeField]
		private int m_enemiesTotal; //Total number of enemies

		[SerializeField]
		private int m_endOfSceneRewards;

		[SerializeField]
		public GameRound [] m_gameRounds;

		[SerializeField]
		public bool m_isRunning = true;

		[SerializeField]
		public bool m_isDisplayingClock;

		[SerializeField]
		private UILabel m_clockLable;

		[SerializeField]
		private UILabel m_messageLable;

		[SerializeField]
		private EventHandler m_gameOverRound;

		[SerializeField]
		private bool m_isTran;
	
		#region Properties

		public int EnemiesKilledTotal
		{
			get { return m_enemiesKilledTotal; }
			set { m_enemiesKilledTotal = value; }
		}

		public int EnemiesTotal
		{
			get { return m_enemiesTotal; }
			set { m_enemiesTotal = value; }
		}

		public int NumberOfRounds
		{
			get { return m_numberOfRounds; }
			set { m_numberOfRounds = value; }
		}

		public int CurrentRound
		{
			get { return m_currentRound; }
			set { m_currentRound = value; }
		}

		public int EndOfSceneRewards
		{
			get { return m_endOfSceneRewards; }
			set { m_endOfSceneRewards = value; }
		}

		#endregion
	

		void Awake ()
		{
			m_numberOfRounds = m_gameRounds.Length;
		}

		public void displayClock()
		{
			m_isDisplayingClock = true;
		}

		public void unDisplayClock()
		{
			m_isDisplayingClock = false;
		}

		public void toggleClock()
		{
			m_isDisplayingClock = !m_isDisplayingClock;
		}

		// Use this for initialization
		void Start () 
		{
			m_clockLable = GameObject.Find("lableClock").GetComponent<UILabel>();
			m_messageLable = GameObject.Find("Label-Message").GetComponent<UILabel>();
			NotificationCenter.DefaultCenter.AddObserver(this, "displayClock");
			NotificationCenter.DefaultCenter.AddObserver(this, "unDisplayClock");
			NotificationCenter.DefaultCenter.AddObserver(this, "toggleClock");
			NotificationCenter.DefaultCenter.AddObserver(this, "updateText");
			NotificationCenter.DefaultCenter.AddObserver(this, "goToGameOverLost");
			//set the defaults
			m_currentRound = 0;
			m_enemiesKilledTotal = 0;
			m_enemiesTotal = 0;
			m_isRunning = true;
			m_isTran = false;

			foreach(GameRound r in m_gameRounds) {
				r.setUp();
			}


		}

		public void updateText( NotificationCenter.Notification p_not )
		{
			if((string)p_not.data["stringID"] == "message") {
				m_messageLable.text =  (string)p_not.data["data"];
			}
		}
		
		// Update is called once per frame
		void Update () 
		{

			if(m_isRunning == true) {

				if(m_currentRound < m_gameRounds.Length) {
					m_gameRounds[m_currentRound].runEvent();
					checkForLastEventKills();
					m_gameRounds[m_currentRound].runCheckForEndOfRound();
					if(m_gameRounds[m_currentRound].IsRoundOver == true) {
						if(m_gameRounds[m_currentRound].IsGoalReached == true) {
							m_currentRound++;
						} else {
							m_isRunning = false;
						}
					}
				} 

				if(m_currentRound <= m_gameRounds.Length -1) {
					if(m_isDisplayingClock == true) {
						if(m_clockLable == null) {
							sendMessage("updateText","clock",m_gameRounds[m_currentRound].Clock.Counter.ToString("00"));
						} else {
							m_clockLable.text = m_gameRounds[m_currentRound].Clock.Counter.ToString("00");
						}
					} else {
						if(m_clockLable != null) {
							sendMessage("updateText","clock","00");
							m_clockLable.text = "00";
						}
					}
				}
			} else {
				sendMessage("updateText","message","Ya'll Need To Hit More Ducks!!");

				StartCoroutine(delayGomeOverSwith(5.0f));
			}
		}

		public void incrementCurrentRoundKills()
		{
			if(m_currentRound < m_gameRounds.Length) {
				m_gameRounds[m_currentRound].CurrentEnemiesKilled++;
				m_gameRounds[m_currentRound].CurrentEventKills++;
				m_gameRounds[m_currentRound].TotalNumOfEnemies += m_gameRounds[m_currentRound].CurrentEnemiesKilled;

			}
		}

		void sendMessage(string p_funcCall, string p_id, string p_data)
		{
			Hashtable dat = new Hashtable();
			dat.Add("stringID",p_id);
			dat.Add("data",p_data);
			NotificationCenter.DefaultCenter.PostNotification(this,p_funcCall,dat);
		}

	 	bool checkForGoals()
		{
			return m_gameRounds[m_currentRound].IsGoalReached;
		}

		void checkForLastEventKills()
		{
			if(m_gameRounds[m_currentRound].CurrentEventKills >= 3) {
				m_gameRounds[m_currentRound].CurrentEventKills = 0;
				Hashtable dat = new Hashtable();
				dat.Add("data","shellPart");
				dat.Add("stringID","99");
				dat.Add("intID",99);
				NotificationCenter.DefaultCenter.PostNotification (null,"spawn",dat);
				GunClip.Instance.Clips++;
			}
		}

		public void goToGameOverLost()
		{
			Application.LoadLevel(4);
		}

		IEnumerator delayGomeOverSwith(float p_sec)
		{
			if(m_isTran == false) {
				m_isTran = true;
				yield return new WaitForSeconds(p_sec);
				Application.LoadLevel(4);
			}
		}
	}

}