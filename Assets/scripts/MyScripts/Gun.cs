﻿
/*
 * Decided to decouple the gun so in case I have different variation of the gun
 * 
 */
using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Gun : MonoBehaviour {

		[SerializeField]
		private GunClip m_clip;

		[SerializeField]
		private float m_rateOfFire;

		[SerializeField]
		public GameObject m_bulletLoc;

		[SerializeField]
		private string m_gunSmoke;

		#region Properties
		public GunClip Clip
		{
			get {return m_clip;}
			set {m_clip = value;}
		}
		#endregion

		public void fire()
		{
			if(m_clip != null) {

				if(!m_clip.empty()) {
					m_clip.CurrentAmmo--;
					GameObject bullet = m_clip.feedBullet();
					setBullet(bullet);
					GameObject smoke = MemoryPool.Instance.findAndGetObjs(m_gunSmoke,false);
					setSmoke(smoke);

				} else {
					m_clip.reload();
				}
			}
		}

		public void setSmoke(GameObject p_smoke)
		{
			if (p_smoke != null) {
				p_smoke.SetActive (true);
				//set the position
				p_smoke.transform.position = m_bulletLoc.transform.position;
				var rot = p_smoke.transform.eulerAngles;
				rot.z = transform.eulerAngles.z - 90f;
				p_smoke.transform.eulerAngles = rot;
			}
		}

		public void setBullet(GameObject p_bullet)
		{
			if(p_bullet != null) {
				
				p_bullet.SetActive(true);
				//set the position
				p_bullet.transform.position = m_bulletLoc.transform.position;
				p_bullet.GetComponent<Bullet>().CallingObj = this.gameObject;

				var rot = p_bullet.transform.eulerAngles;
				rot.z = transform.eulerAngles.z - 90f;
				p_bullet.transform.eulerAngles = rot;
				
				float x =  Mathf.Cos( transform.eulerAngles.z * Mathf.Deg2Rad);
				float y =  Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad);

				p_bullet.rigidbody2D.velocity = new Vector2(x,y) * p_bullet.GetComponent<Bullet>().Speed;
			}
		}
	}
}