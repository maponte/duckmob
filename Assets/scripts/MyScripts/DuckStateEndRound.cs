﻿using UnityEngine;
using System.Collections;
using BoogieDownGames;

public sealed class DuckStateEndRound :  FSMState<DuckAi> {
	
	static readonly DuckStateEndRound instance = new DuckStateEndRound();
	public static DuckStateEndRound Instance 
	{
		get { return instance; }
	}
	static DuckStateEndRound() { }
	private DuckStateEndRound() { }
	
	
	public override void Enter (DuckAi p_duck)
	{
		
	}
	
	public override void ExecuteOnUpdate (DuckAi p_duck) 
	{
		p_duck.resetScale ();
		p_duck.flyOff();
	}
	
	public override void ExecuteOnFixedUpdate (DuckAi p_duck)
	{
		
	}
	
	public override void Exit(DuckAi p_duck) 
	{
		
		
	}
}