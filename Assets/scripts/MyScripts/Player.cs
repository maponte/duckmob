﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames{

	public class Player : MonoBehaviour
	{

		[SerializeField]
		private int m_score;

		[SerializeField]
		private int m_intID;

		// Use this for initialization
		void Start () 
		{
			NotificationCenter.DefaultCenter.AddObserver(this,"updateScoreNot");
		}
		
		// Update is called once per frame
		void Update () 
		{
		
		}

		void OnEnable()
		{
			NotificationCenter.DefaultCenter.AddObserver(this,"updateScore");
		}

		public void updateScoreNot(NotificationCenter.Notification p_note)
		{
			if(p_note.data.ContainsKey("intID")) {
				if(m_intID == (int)p_note.data["intID"]) {
					m_score += (int)p_note.data["score"];

					Hashtable dat = new Hashtable();
					dat.Add("stringID","M");
					dat.Add("data",m_score.ToString());
					NotificationCenter.DefaultCenter.PostNotification(this,"updateText",dat);
				}
			}
		}

		public void updateScore( int p_score)
		{
			m_score += p_score;
		}
	}
}