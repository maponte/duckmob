﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Clouds : MonoBehaviour 
	{

		[SerializeField]
		private Vector2 m_velocity;

		// Use this for initialization
		void Start () 
		{
		
		}

		void OnEnable()
		{

		}
		
		// Update is called once per frame
		void Update ()
		{
			rigidbody2D.velocity = m_velocity;
		}
	}
}