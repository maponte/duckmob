﻿/*
 * Author: Misael Aponte
 * Purpose: Script to run the NGui Menu 
 * Description:  Decided to create a one for menu system using Ngui.
 * It toggles the enable individual menu systems according to the level your on and the 
 * user inputs
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BoogieDownGames {

	public class MenuSystem : UnitySingletonPersistent<MenuSystem> 
	{

		[SerializeField]
		private List<Gui> m_guiList;

		[SerializeField]
		private TimeKeeper m_clock;

		[SerializeField]
		private bool m_isPaused;

		[SerializeField]
		private AudioListener m_listner;

		[SerializeField]
		private UISlider m_volumeSlider;

		[SerializeField]
		private UISlider m_volumeSliderPause;

		[SerializeField]
		private float m_masterVolume;

		[SerializeField]
		private bool m_isAdBased;

		[SerializeField]
		private string m_adId;



		private FiniteStateMachine<MenuSystem> m_fsm = new FiniteStateMachine<MenuSystem>();


		#region PROPERTIES

		
		public bool IsAdBased
		{
			get { return m_isAdBased;}
		}

		public FiniteStateMachine<MenuSystem> FSM
		{
			get { return m_fsm; }
		}

		public List<Gui> GuiList
		{
			get { return m_guiList; }
		}

		public TimeKeeper Clock
		{
			get { return m_clock; }
		}

		#endregion

		// Use this for initialization
		void Start () 
		{
			if(m_listner == null) {
				m_listner = GetComponent<AudioListener>();
			}
			setDefaults();
			m_clock.startClock();
			NotificationCenter.DefaultCenter.AddObserver(this, "switchToGameOver");
			m_fsm.Configure(this,MenuStateSetup.Instance);
		}
		
		// Update is called once per frame
		void Update () 
		{
			m_fsm.runOnUpdate();
			if(Input.GetKeyDown(KeyCode.A)) {
				if(m_isPaused == false) {
					gameModePause();
				} else {
					gameModeRun();
				}
			}

			if(Input.GetKeyDown(KeyCode.L)) {
				this.switchToWinGame();
			}
		}

		void FixedUpdate ()
		{
			m_fsm.runOnFixedUpdate();
		}

		//Sets the default menu status
		public void setDefaults()
		{
			foreach(Gui gui in m_guiList) {
				gui.setDefault();
			}
		}

		public void setUpMenus (int p_level)
		{
			foreach(Gui gui in m_guiList) {
				//Debug.LogError("Seting up level => " + p_level);
				//Debug.LogError("Gui Name => " + gui.Name + " set to => " + gui.SceneSettings[p_level].IsEnabled);
				gui.enabler( gui.SceneSettings[p_level].IsEnabled );
			}
			
		}

		public void switchToGame()
		{
			m_fsm.ChangeState(MenuStateGame.Instance);
		}

		public void gameModePause()
		{
			Time.timeScale = 0.0f;
			m_isPaused = true;
		}
		
		public void gameModeRun() 
		{
			Time.timeScale = 1.0f;
			m_isPaused = false;
		}

		public void tooglePause ()
		{
			if(m_isPaused == false) {
				gameModePause();
			} else {
				gameModeRun();
			}
		}

		public void switchToGameOver()
		{
			Application.LoadLevel(3);
		}

		public void switchToGameOverLost()
		{
			Application.LoadLevel(4);
		}

		public void sendEasterEggMessage()
		{
			NotificationCenter.DefaultCenter.PostNotification(this,"spawnDel");
		}

		public void switchToWinGame()
		{
			Application.LoadLevel(6);
		}

		void OnLevelWasLoaded(int level) 
		{


			//m_guiList[2].disable();
			setUpMenus(level);
			Debug.LogError("Ive loaded " + level);

			if(level ==0) {

				//m_guiList[0].enable();
			}
			if(level == 2) {
				m_guiList[2].enable();

			} else if(level == 3 || level == 4) {

			}
			
			if(level == 1) {
				displayAd();
			} 
			
			if(level == 3) {
				displayAd();
			}
			
			if(level == 4) {
				displayAd();
			}
			if(level == 5) {
				m_guiList[2].enable();
			}

			if(level == 6) {
				displayAd();
			}
			NotificationCenter.DefaultCenter.AddObserver(this, "switchToGameOver");
			NotificationCenter.DefaultCenter.AddObserver(this, "switchToWinGame");
		}
		
		public void displayAd()
		{
			if(m_isAdBased) {
				if(ApplifierImpactMobile.canShowCampaigns() && ApplifierImpactMobile.canShowImpact()) {
					ApplifierImpactMobile.showImpact(m_adId);
				}
			}
		}

		public void goToMenu()
		{
			m_fsm.ChangeState(MenuStateMainMenu.Instance);
		}

		public void adJustSound()
		{
			if(m_volumeSlider != null) {
				AudioListener.volume = m_volumeSlider.value;
				m_volumeSliderPause.value = AudioListener.volume;
			}
		}

		public void adJustSoundPause()
		{
			if(m_volumeSliderPause != null) {
				AudioListener.volume = m_volumeSliderPause.value;
				m_volumeSlider.value = AudioListener.volume;
			}
		}

		public void endGame()
		{
			GameMaster.Instance.save();
			Application.Quit();
		}

	}
}
