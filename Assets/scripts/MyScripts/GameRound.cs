﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames {

	[Serializable]
	public class GameRound : PropertyAttribute 
	{

		[SerializeField]
		private string m_name;

		enum EndOfRoundEnum { Timer, NumOfEnem, Both };

		[SerializeField]
		private EndOfRoundEnum m_endOfRoundCriteria = EndOfRoundEnum.Both;

		[SerializeField]
		private TimeKeeper m_GameRoundClock;
		
		[SerializeField]
		private int m_totalNumOfEnemies; //Total number of enemies for this round

		[SerializeField]
		private int m_currentEnemiesKilled = 0;

		[SerializeField]
		private int m_KillGoal;//The number of kills you need to win the round

		[SerializeField]
		private int m_reward; //Reward for finishing the round

		[SerializeField]
		private bool m_isRoundOver = false;

		[SerializeField]
		private bool m_isGoalReached;

		[SerializeField]
		private int m_currentEventKills;

		[SerializeField]
		private List<EventHandler> m_events;

		[SerializeField]
		private int m_currentEventIndex;

		private delegate void roundTypeDelegate();
		private roundTypeDelegate m_roundTypeDelegate;


		#region Properties

		public int CurrentEventKills
		{
			get { return m_currentEventKills; }
			set { m_currentEventKills  = value; }
		}

		public int TotalNumOfEnemies
		{
			get { return m_totalNumOfEnemies; }
			set { m_totalNumOfEnemies = value; }
		}

		public int CurrentEnemiesKilled
		{
			get { return m_currentEnemiesKilled; }
			set { m_currentEnemiesKilled = value; }
		}

		public int Reward
		{
			get { return m_reward; }
			set { m_reward = value; }
		}

		public bool IsRoundOver
		{
			get { return m_isRoundOver; }
			set { m_isRoundOver = value; }
		}

		public TimeKeeper Clock
		{
			get { return m_GameRoundClock; }
		}

		public bool IsGoalReached
		{
			get {return m_isGoalReached;}
		}

		#endregion
	
		void sendMessage(string p_funcCall, string p_id, string p_data)
		{
			Hashtable dat = new Hashtable();
			dat.Add("stringID",p_id);
			dat.Add("data",p_data);
			NotificationCenter.DefaultCenter.PostNotification(null,p_funcCall,dat);
		}

		public void runEvents()
		{
			foreach(EventHandler e in m_events) {
				e.runActions();
			}
		}

		public void runEvent()
		{
			if(m_currentEventIndex <= m_events.Count-1) {

				if((int)m_events[m_currentEventIndex].Clock.LoopType == 0) {
					if(m_events[m_currentEventIndex].m_isDone == true) {
						m_currentEventIndex++;
					}
				} else {
					m_events[m_currentEventIndex].m_isDone = false;
				}
				m_events[m_currentEventIndex].runActions();
			}
		}

		public void setUp()
		{
			foreach(EventHandler e in m_events) {
				e.setUp();
			}
			switch(m_endOfRoundCriteria) {
				
			case EndOfRoundEnum.Both:
				m_GameRoundClock.startClock();
				m_roundTypeDelegate = endOfRoundCheckBoth;
				break;
				
			case EndOfRoundEnum.NumOfEnem:
				m_roundTypeDelegate = endOfRoundCheckEnemies;
				break;
				
			case EndOfRoundEnum.Timer:
				m_GameRoundClock.startClock();
				m_roundTypeDelegate = endOfRoundCheckClock;
				break;
			}

			m_isRoundOver = false;
		}

		public void runCheckForEndOfRound()
		{
			m_roundTypeDelegate();
		}

		private void endOfRoundCheckEnemies()
		{
			if(m_currentEnemiesKilled < m_KillGoal) {
				m_isGoalReached = false;
			} else {
				m_isGoalReached = true;
			}
		}

		private void endOfRoundCheckClock()
		{
			m_GameRoundClock.run();
			if(m_GameRoundClock.m_trigger == true) {
				m_isRoundOver = true;
				m_isGoalReached = true;
			}
		}

		private void endOfRoundCheckBoth()
		{
			endOfRoundCheckClock();
			endOfRoundCheckEnemies();
		}
	}
}