﻿/*
 * Misael Aponte Feb 10, 2014
 * A spawn point for the game
 * 
 */
using UnityEngine;
using System.Collections;

namespace BoogieDownGames 
{
	public class SpawnPoint : MonoBehaviour 
	{
		[SerializeField]
		private string m_nameOfObj;

		[SerializeField]
		private GameObject m_obj;

		[SerializeField]
		private bool m_isOn = true;

		[SerializeField]
		private string m_strID;

		[SerializeField]
		private int m_intID;




		private enum IDTYPE { String, Int };
		[SerializeField]
		private IDTYPE m_idType = IDTYPE.String;

		[SerializeField]
		private Vector2 m_velocity;

		[SerializeField]
		private int m_layer;

		[SerializeField]
		private bool m_isVelocityRandom;

		[SerializeField]
		private int m_amount;

		// Use this for initialization
		void Start () 
		{
			NotificationCenter.DefaultCenter.AddObserver(this,"spawn");
			NotificationCenter.DefaultCenter.AddObserver(this,"spawnDel");
			if(m_obj != null) {
				m_nameOfObj = m_obj.name;
			}
		}
		
		public void spawn( NotificationCenter.Notification p_not)
		{
			if(m_isOn) {
				switch(m_idType) {
				
				case IDTYPE.String:
					if(p_not.data.ContainsKey("stringID")) {
						if(m_strID == (string)p_not.data["stringID"]) {
							setObj(MemoryPool.Instance.findAndGetObjs(m_nameOfObj,false));
						}
					}
					break;
				case IDTYPE.Int:
					if(p_not.data.ContainsKey("intID")) {
						if(m_intID == (int)p_not.data["intID"]) {
							setObj(MemoryPool.Instance.findAndGetObjs(m_nameOfObj,false));
						}
					}
					break;
				}
			}
		}

		public void setObj(GameObject obj)
		{
			if(obj != null) {
				if(m_isVelocityRandom) {
					m_velocity =  new Vector2(Random.Range(-8,8),Random.Range(-8,8));
				}
				obj.SetActive(true);
				obj.transform.position = transform.position;
				DuckAi d = obj.GetComponent<DuckAi>();
				if(d != null) {
					d.HappyVel = m_velocity;
				}
				obj.gameObject.layer = m_layer;
			}
		}

		public void spawnDel()
		{
			for(int index = 0; index < m_amount; index++) {
				setObj(MemoryPool.Instance.findAndGetObjs(m_nameOfObj,false));
			}
		}
	}
}