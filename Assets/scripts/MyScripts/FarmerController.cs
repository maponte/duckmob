﻿using UnityEngine;
using System.Collections;


namespace BoogieDownGames {

	public class FarmerController : MonoBehaviour {

		[SerializeField]
		private Animator m_anim;

		public void shoot()
		{
			m_anim.SetTrigger("shoot");
		}

		public void cleanGun()
		{
			m_anim.SetTrigger("clean");
		}

		public void restock()
		{
			m_anim.SetTrigger("restock");
		}
	}
}