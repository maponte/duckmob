﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {
		
	public class Bullet : MonoBehaviour 
	{
		[SerializeField]
		private float m_speed;

		[SerializeField]
		private int m_damage;

		//Where to set the object and rotation
		[SerializeField]
		private GameObject m_callingObj;

		[SerializeField]
		private Animator m_anim;

		#region PROPERTIES

		public int Damage
		{
			get { return m_damage; }
		}

		public float Speed
		{
			get { return m_speed; }
		}
		#endregion
	

		public GameObject CallingObj
		{
			get { return m_callingObj; }
			set { m_callingObj = value; }
		}


		void OnBecameInvisible()
		{
			gameObject.SetActive(false);
		}

	}
}