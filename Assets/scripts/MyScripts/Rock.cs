﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class Rock : MonoBehaviour {

		[SerializeField]
		private string m_particle;
	
		void OnCollisionEnter2D(Collision2D coll) 
		{
			if(coll.gameObject.name == "Player3D") {
				GameObject obj = MemoryPool.Instance.findAndGetObjs(m_particle,false);
				obj.transform.position = transform.position;
				obj.particleSystem.Play();
				gameObject.SetActive(false);
			}
		}
	}
}