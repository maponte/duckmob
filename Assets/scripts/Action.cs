﻿/*
 * Misael Aponte Feb 6, 2014
 * This stores an event action so that we can do something with it
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames{

	[Serializable]
	public class Action : PropertyAttribute {

		[SerializeField]
		private string m_post;

		[SerializeField]
		private string m_strID;

		[SerializeField]
		private int m_intID;

		[SerializeField]
		private bool m_isRandom;

		[Range(0, 100)]
		public int m_rangeMin;

		[Range(0, 100)]
		public int m_rangeMax;

		public bool m_isDisplayClock;


		[SerializeField]
		private string m_funcCall;

		public string Posts
		{
			get { return m_post; }
			set { m_post = value; }
		}

		public void startAction()
		{
			if(m_funcCall != null) {
				Hashtable dat = new Hashtable();
				dat.Add("data",m_post);
				dat.Add("stringID",m_strID);
				if(m_isRandom) {

					m_intID = UnityEngine.Random.Range(m_rangeMin,m_rangeMax);
					dat.Add("intID",m_intID);
				} else {
					dat.Add("intID",m_intID);
				}
				NotificationCenter.DefaultCenter.PostNotification (null,m_funcCall,dat);

				if(m_isDisplayClock == true) {
					NotificationCenter.DefaultCenter.PostNotification(null, "displayClock");
				} else {
					NotificationCenter.DefaultCenter.PostNotification(null, "unDisplayClock");
				}
			}
		}
	}
}