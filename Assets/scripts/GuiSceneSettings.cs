﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BoogieDownGames {
	[Serializable]
	public class GuiSceneSettings : PropertyAttribute 
	{
		[SerializeField]
		private int m_sceneNumber;

		[SerializeField]
		private string m_sceneName;

		[SerializeField]
		private bool m_isEnabled;

		#region PROPERTIES

		public int SceneNumber
		{
			get { return m_sceneNumber; }
		}

		public string SceneName
		{
			get { return m_sceneName; }
		}

		public bool IsEnabled
		{
			get { return m_isEnabled; }
		}

		#endregion
	}
}