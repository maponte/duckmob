using UnityEngine;
using System.Collections;
using BoogieDownGames;

public class GameControl : MonoBehaviour
{
	public PlayerController m_controls;


	void Start()
	{
#if UNITY_STANDALONE
		gameObject.SetActive(false);
#endif
	}
	
	void OnJoystick(Vector2 delta)
	{
		m_controls.rotate(0,0,delta.x);
	}
	
	
	void OnJoystickRotation(Vector2 rot)
	{

	}
	
	
	void OnClick()
	{

	}

}
