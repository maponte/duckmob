﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames{

	public class Firecontrol : MonoBehaviour {

		public PlayerController m_controls;
		[SerializeField]
		private bool m_canFire = true;
		[SerializeField]
		private float p_rof;

		void Start()
		{
			#if UNITY_STANDALONE
			gameObject.SetActive(false);
			#endif
		}

		void OnJoystick(Vector2 delta)
		{
			StartCoroutine(delay(p_rof));
		}

		IEnumerator delay (float p_sec)
		{
			if(m_canFire == true) {
				m_canFire = false;
				m_controls.shoot();
				yield return new WaitForSeconds(p_sec);
				m_canFire = true;
			}
		}
	}

}