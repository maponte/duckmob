﻿/*
 * This controls the button for the player only just calls the method that need to be run
 */

using UnityEngine;
using System.Collections;

namespace BoogieDownGames {
	
	public class ButtonControlBase : MonoBehaviour 
	{

		[SerializeField]
		private PlayerController m_controls;

		void OnClick()
		{
			NotificationCenter.DefaultCenter.PostNotification(this,"shoot");
			//m_controls.shoot();
		}
	}
}