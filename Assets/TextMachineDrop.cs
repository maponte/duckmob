﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {
	public class TextMachineDrop : MonoBehaviour {

		[SerializeField]
		private GameMaster m_GameMaster;
		[SerializeField]
		private UILabel m_label;

		// Use this for initialization
		void Start ()
		{
		
		}

		void OnEnable()
		{
			if(m_GameMaster != null) {
				m_label.text  = "High Score: " + m_GameMaster.HighScore.ToString();
			}
		}
		
		// Update is called once per frame
		void Update () 
		{

		}
	}
}